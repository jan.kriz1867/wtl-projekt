from django.apps import AppConfig


class ZpravyConfig(AppConfig):
    name = 'zpravy'
